libcss-packer-perl (2.09-2) unstable; urgency=medium

  * Remove generated test files via debian/clean. (Closes: #1049211)
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Tue, 05 Mar 2024 17:56:12 +0100

libcss-packer-perl (2.09-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.0, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 2.09.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Mon, 26 Jun 2023 18:56:49 +0200

libcss-packer-perl (2.08-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-
    Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Import upstream version 2.08.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Drop unneeded version constraints from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Sun, 14 Mar 2021 17:17:02 +0100

libcss-packer-perl (2.07-1) unstable; urgency=medium

  * Import upstream version 2.07.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.2.1.

 -- gregor herrmann <gregoa@debian.org>  Wed, 21 Nov 2018 19:00:41 +0100

libcss-packer-perl (2.05-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 2.05.
  * Add new build dependency: libtest-file-contents-perl.
  * Enable another test by adding libtest-memory-cycle-perl to B-D-I.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Mar 2018 17:23:37 +0100

libcss-packer-perl (2.03-1) unstable; urgency=medium

  [ Jotam Jr. Trejo ]
  * New upstream release.
  * Update debian/copyright: upstream moved to ExtUtils::MakeMaker,
    inc/Modules/* was removed.
  * Bump standards-version to 4.1.0.
  * Bump debhelper compatibility level to 10.

 -- Jotam Jr. Trejo <jotamjr@debian.org.sv>  Mon, 25 Sep 2017 18:48:13 -0600

libcss-packer-perl (2.02-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import upstream version 2.00
  * Update years of M::I copyright
  * Mark package autopkgtest-able

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.

  * Import upstream version 2.02.
  * Update upstream maintainer in d/copyright, d/upstream/metadata.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.8.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Fri, 03 Jun 2016 22:42:15 +0200

libcss-packer-perl (1.002001-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * New upstream release.
  * Update years of packaging copyright.
  * debian/copyright: update to Copyright-Format 1.0.
  * Bump Standards-Version to 3.9.3 (no changes).
  * Remove debian/clean, test files are removed by upstream build system.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Mar 2012 18:13:09 +0100

libcss-packer-perl (1.002-1) unstable; urgency=low

  * New upstream release
  * debian/control: update version dependency for libregexp-reggrp-perl
  * debian/changelog: update copyright statement years for files under
    inc/Module/*
  * Bump Standards Version to 3.9.2 (no changes)
  * Add myself to Uploaders and Copyright
  * Bump DH compat to 8

 -- Jotam Jr. Trejo <jotamjr@debian.org.sv>  Sun, 12 Jun 2011 17:00:10 -0600

libcss-packer-perl (1.000001-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.
  * Switch to source format 3.0 (quilt).
  * debian/copyright: update formatting.
  * Add /me to Uploaders.

  [ Ernesto Hernández-Novich (USB) ]
  * New upstream release
  * debian/copyright: added Module::Install stanza; updated copyright
    dates for upstream and packaging.
  * debian/control: added versioned Dependens on libregexp-reggrp-perl;
    updated Standards-Version.

  [ Jonathan Yu ]
  * New upstream release

 -- gregor herrmann <gregoa@debian.org>  Sat, 12 Mar 2011 17:57:14 +0100

libcss-packer-perl (0.2-1) unstable; urgency=low

  * Initial Release. (Closes: #546820)

 -- Ernesto Hernández-Novich (USB) <emhn@usb.ve>  Tue, 15 Sep 2009 18:23:59 -0430
